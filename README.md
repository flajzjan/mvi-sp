# MVI SP
author: flajzjan@cvut.cz
# Understanding of long texts
The aim of this semestral work is to research several existing libraries for generating long text embeddings.
The libraries are then compared using availible benchmark datasets. 
Then the examined libraries are used in text classification task to simulate their usability in practice.

# Summary of work
We selected 3 libraries for performing benchmarks based on bitext retrieval using 6 different languages:
- LaBSE
- mUSE
- XLM-Base

We performed this benchmark on two existing dataset:
- Tatoeba which we adjusted for our needs
- BUCC which we used as it is

In this benchmarks the performance of LaBSE and mUSE were slightly better than XLM. 
So we used them for classifing the sentiment of texts from Massive Multilingual Sentiment Corpora which we reduced four our needs.

Finally, we performed a classification using those two libraries and a simple neural network using which LaBSE slightly outperformed mUSE.



# Repository description
This repository contains my semestral projet from NI-MVI called Understanding of long texts. It is divided to several folders:
- [results/](https://gitlab.fit.cvut.cz/flajzjan/mvi-sp/-/tree/main/results?ref_type=heads) contains the images and tables used in the report
- [src/](https://gitlab.fit.cvut.cz/flajzjan/mvi-sp/-/tree/main/src?ref_type=heads) contains implementation notebooks and datasets

Files:
- MVI_milestone.pdf contains the semestral milestone 
- report.pdf is final report of this project



# Notebooks
- [src/mvi-semestral_benchmark](https://gitlab.fit.cvut.cz/flajzjan/mvi-sp/-/blob/main/src/mvi-semestral_benchmark.ipynb?ref_type=heads) contains the implementation of benchmarks on all of the libraries. Both libraries and datasets are downloaded directly to the notebook while running.
- [src/mvi-classification contains](https://gitlab.fit.cvut.cz/flajzjan/mvi-sp/-/blob/main/src/mvi-classification.ipynb?ref_type=heads) the implementation of classification task using the libraries. It imports the dataset lng_final_dataset.csv which is in the directory and uploaded to Kaggle.
- [src/mvi-dataset-analysis](https://gitlab.fit.cvut.cz/flajzjan/mvi-sp/-/blob/main/src/mvi-dataset-analysis.ipynb?ref_type=heads) contains the preprocessing of the MMS dataset. Since the dataset is protected on huggingface, we need a token to download it. Notebook that automatically downloads the dataset is not included in this repo. Only file dataset.csv contatins the dataset stripped of languages we don't need.

Notebook [mvi-milestone](https://gitlab.fit.cvut.cz/flajzjan/mvi-sp/-/blob/main/src/mvi-milestone.ipynb?ref_type=heads) is not a part of the final solution, it served for performing pilot experiments 

# Running the notebooks
All of the notebooks were created in Kaggle and are publicly available by clicking following links:

- [mvi-semestral_benchmark](https://www.kaggle.com/code/janflajk/mvi-semestral)
- [mvi-classification](https://www.kaggle.com/code/janflajk/mvi-classification)
- [mvi-dataset-analysis](https://www.kaggle.com/code/janflajk/mvi-dataset-analysis)

The relative path for loading the datasets differ on Kaggle and in repo for obvious reasons. You can run them locally in the repo or on Kaggle.